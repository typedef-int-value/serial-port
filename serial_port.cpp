/*
 * File:   serial_port.cpp
 * Author: Terraneo Federico
 * Distributed under the Boost Software License, Version 1.0.
 * Created on September 12, 2009, 3:47 PM
 *
 * v1.06: C++11 support
 *
 * v1.05: Fixed a bug regarding reading after a timeout (again).
 *
 * v1.04: Fixed bug with timeout set to zero
 *
 * v1.03: Fix for Mac OS X, now fully working on Mac.
 *
 * v1.02: Code cleanup, speed improvements, bug fixes.
 *
 * v1.01: Fixed a bug that caused errors while reading after a timeout.
 *
 * v1.00: First release.
 */

#include "serial_port.h"

#include <algorithm>
#include <boost/bind.hpp>
#include <iostream>
#include <string>

namespace serial {
using namespace std;
using namespace boost;

serial_port::serial_port()
    : io_(), port_(io_), timer_(io_), timeout_(boost::posix_time::seconds(0)) {}

serial_port::serial_port(const std::string& devname, unsigned int baud_rate,
                         asio::serial_port_base::parity opt_parity,
                         asio::serial_port_base::character_size opt_csize,
                         asio::serial_port_base::flow_control opt_flow,
                         asio::serial_port_base::stop_bits opt_stop)
    : io_(), port_(io_), timer_(io_), timeout_(boost::posix_time::seconds(0)) {
  open(devname, baud_rate, opt_parity, opt_csize, opt_flow, opt_stop);
}

void serial_port::open(const std::string& devname, unsigned int baud_rate,
                       asio::serial_port_base::parity opt_parity,
                       asio::serial_port_base::character_size opt_csize,
                       asio::serial_port_base::flow_control opt_flow,
                       asio::serial_port_base::stop_bits opt_stop) {
  if (is_open()) close();
  port_.open(devname);
  port_.set_option(asio::serial_port_base::baud_rate(baud_rate));
  port_.set_option(opt_parity);
  port_.set_option(opt_csize);
  port_.set_option(opt_flow);
  port_.set_option(opt_stop);
}

bool serial_port::is_open() const { return port_.is_open(); }

void serial_port::close() {
  if (is_open() == false) return;
  port_.close();
}

void serial_port::set_timeout(const boost::posix_time::time_duration& t) {
  timeout_ = t;
}

void serial_port::write(const std::vector<unsigned char>& data) {
  write((char*)data.data(), data.size());
}

void serial_port::write(const char* data, size_t size) {
  asio::write(port_, asio::buffer(data, size));
}

void serial_port::write(const std::vector<char>& data) {
  asio::write(port_, asio::buffer(&data[0], data.size()));
}

void serial_port::write(const std::string& s) {
  asio::write(port_, asio::buffer(s.c_str(), s.size()));
}

void serial_port::read(char* data, size_t size) {
  read_setup_ = read_setup(data, size);
  asio::async_read(
      port_, asio::buffer(read_setup_.data_, read_setup_.size_),
      boost::bind(&serial_port::read_complete, this, asio::placeholders::error,
                  asio::placeholders::bytes_transferred));

  // For this code to work, there should always be a timeout, so the
  // request for no timeout is translated into a very long timeout
  if (timeout_ != boost::posix_time::seconds(0))
    timer_.expires_from_now(timeout_);
  else
    timer_.expires_from_now(boost::posix_time::hours(100000));

  timer_.async_wait(boost::bind(&serial_port::timeout_expired, this,
                                asio::placeholders::error));

  read_result_ = read_result::in_progress;
  for (;;) {
    io_.run_one();
    switch (read_result_) {
      case read_result::success:
        timer_.cancel();
        return;
      case read_result::timeout:
        port_.cancel();
        throw(timeout_exception("timeout expired"));
      case read_result::error:
        timer_.cancel();
        port_.cancel();
        throw(boost::system::system_error(boost::system::error_code(),
                                          "Error while reading"));
        // if read_result::in_progress remain in the loop
    }
  }
}
std::vector<char> serial_port::read_until_timeout(size_t max_capacity) {
  std::vector<char> data;
  if (timeout_ == boost::posix_time::seconds(0)) return data;

  try {
    while (true) {
      char ch = 0;
      read(&ch, 1);
      data.push_back(ch);

      if (data.size() >= max_capacity) break;
    }
  } catch (const timeout_exception&) {
  }

  return data;
}

void serial_port::set_baudrate(int bps) {
  port_.set_option(asio::serial_port_base::baud_rate(bps));
}

serial_port::~serial_port() { close(); }

void serial_port::timeout_expired(const boost::system::error_code& error) {
  if (!error && read_result_ == read_result::in_progress)
    read_result_ = read_result::timeout;
}

void serial_port::read_complete(const boost::system::error_code& error,
                                const size_t byte_transferred) {
  if (!error) {
    read_result_ = read_result::success;
    return;
  }

// In case a asynchronous operation is cancelled due to a timeout,
// each OS seems to have its way to react.
#ifdef _WIN32
  if (error.value() == 995) return;  // Windows spits out error 995
#elif defined(__APPLE__)
  if (error.value() == 45) {
    // Bug on OS X, it might be necessary to repeat the setup
    // http://osdir.com/ml/lib.boost.asio.user/2008-08/msg00004.html
    asio::async_read(port_, asio::buffer(read_setup_.data_, read_setup_.size_),
                     boost::bind(&serial_port::read_complete, this,
                                 asio::placeholders::error,
                                 asio::placeholders::bytes_transferred));
    return;
  }
#else  // Linux
  if (error.value() == 125) return;  // Linux outputs error 125
#endif

  read_result_ = read_result::error;
}

}  // namespace serial
