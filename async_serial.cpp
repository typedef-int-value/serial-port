/*
 * File:   async_serial.cpp
 * Author: Terraneo Federico
 * Distributed under the Boost Software License, Version 1.0.
 * Created on September 7, 2009, 10:46 AM
 *
 * v1.03: C++11 support
 *
 * v1.02: Fixed a bug in BufferedAsyncSerial: Using the default constructor
 * the callback was not set up and reading didn't work.
 *
 * v1.01: Fixed a bug that did not allow to reopen a closed serial port.
 *
 * v1.00: First release.
 *
 * IMPORTANT:
 * on Mac OS X boost asio's serial ports have bugs, and the usual implementation
 * of this class does not work. So a workaround class was written temporarily,
 * until asio (hopefully) will fix Mac compatibility for serial ports.
 * 
 * Please note that unlike said in the documentation on OS X until asio will
 * be fixed serial port *writes* are *not* asynchronous, but at least
 * asynchronous *read* works.
 * In addition the serial port open ignores the following options: parity,
 * character size, flow, stop bits, and defaults to 8N1 format.
 * I know it is bad but at least it's better than nothing.
 *
 */

#include "AsyncSerial.h"

#include <string>
#include <algorithm>
#include <thread>
#include <mutex>
#include <boost/bind.hpp>
#include <boost/shared_array.hpp>

using namespace std;
using namespace boost;

//
//Class async_serial
//

#ifndef __APPLE__

class async_serial_impl: private boost::noncopyable
{
public:
    async_serial_impl(): io(), port(io), bg_thread(), open(false),
            error(false) {}

    boost::asio::io_service io; ///< Io service object
    boost::asio::serial_port port; ///< Serial port object
    std::thread bg_thread; ///< Thread that runs read/write operations
    bool open; ///< True if port open
    bool error; ///< Error flag
    mutable std::mutex error_mutex; ///< Mutex for access to error

    /// Data are queued here before they go in write_buffer
    std::vector<char> write_queue;
    boost::shared_array<char> write_buffer; ///< Data being written
    size_t writer_buffer_size; ///< Size of write_buffer
    std::mutex write_queue_mutex; ///< Mutex for access to write_queue
    char read_buffer[async_serial::read_buffer_size]; ///< data being read

    /// Read complete callback
    std::function<void (const char*, size_t)> callback;
};

async_serial::async_serial(): impl_(new async_serial_impl)
{

}

async_serial::async_serial(const std::string& devname, unsigned int baud_rate,
        asio::serial_port_base::parity opt_parity,
        asio::serial_port_base::character_size opt_csize,
        asio::serial_port_base::flow_control opt_flow,
        asio::serial_port_base::stop_bits opt_stop)
        : impl_(new async_serial_impl)
{
    open(devname,baud_rate,opt_parity,opt_csize,opt_flow,opt_stop);
}

void async_serial::open(const std::string& devname, unsigned int baud_rate,
        asio::serial_port_base::parity opt_parity,
        asio::serial_port_base::character_size opt_csize,
        asio::serial_port_base::flow_control opt_flow,
        asio::serial_port_base::stop_bits opt_stop)
{
    if(is_open()) close();

    set_error_status(true);//If an exception is thrown, error_ remains true
    impl_->port.open(devname);
    impl_->port.set_option(asio::serial_port_base::baud_rate(baud_rate));
    impl_->port.set_option(opt_parity);
    impl_->port.set_option(opt_csize);
    impl_->port.set_option(opt_flow);
    impl_->port.set_option(opt_stop);

    //This gives some work to the io_service before it is started
    impl_->io.post(boost::bind(&async_serial::do_read, this));

    thread t(boost::bind(&asio::io_service::run, &impl_->io));
    impl_->bg_thread.swap(t);
    set_error_status(false);//If we get here, no error
    impl_->open=true; //Port is now open
}

bool async_serial::is_open() const
{
    return impl_->open;
}

bool async_serial::error_status() const
{
    lock_guard<mutex> l(impl_->error_mutex);
    return impl_->error;
}

void async_serial::close()
{
    if(!is_open()) return;

    impl_->open=false;
    impl_->io.post(boost::bind(&async_serial::do_close, this));
    impl_->bg_thread.join();
    impl_->io.reset();
    if(error_status())
    {
        throw(boost::system::system_error(boost::system::error_code(),
                "Error while closing the device"));
    }
}

void async_serial::write(const char *data, size_t size)
{
    {
        lock_guard<mutex> l(impl_->write_queue_mutex);
        impl_->write_queue.insert(impl_->write_queue.end(),data,data+size);
    }
    impl_->io.post(boost::bind(&async_serial::do_write, this));
}

void async_serial::write(const std::vector<char>& data)
{
    {
        lock_guard<mutex> l(impl_->write_queue_mutex);
        impl_->write_queue.insert(impl_->write_queue.end(),data.begin(),
                data.end());
    }
    impl_->io.post(boost::bind(&async_serial::do_write, this));
}

void async_serial::write(const std::string& s)
{
    {
        lock_guard<mutex> l(impl_->write_queue_mutex);
        impl_->write_queue.insert(impl_->write_queue.end(),s.begin(),s.end());
    }
    impl_->io.post(boost::bind(&async_serial::do_write, this));
}

async_serial::~async_serial()
{
    if(is_open())
    {
        try {
            close();
        } catch(...)
        {
            //Don't throw from a destructor
        }
    }
}

void async_serial::do_read()
{
    impl_->port.async_read_some(asio::buffer(impl_->read_buffer,read_buffer_size),
            boost::bind(&async_serial::read_end,
            this,
            asio::placeholders::error,
            asio::placeholders::bytes_transferred));
}

void async_serial::read_end(const boost::system::error_code& error,
        size_t bytes_transferred)
{
    if(error)
    {
        #ifdef __APPLE__
        if(error.value()==45)
        {
            //Bug on OS X, it might be necessary to repeat the setup
            //http://osdir.com/ml/lib.boost.asio.user/2008-08/msg00004.html
            do_read();
            return;
        }
        #endif //__APPLE__
        //error can be true even because the serial port was closed.
        //In this case it is not a real error, so ignore
        if(is_open())
        {
            do_close();
            set_error_status(true);
        }
    } else {
        if(impl_->callback) impl_->callback(impl_->read_buffer,
                bytes_transferred);
        do_read();
    }
}

void async_serial::do_write()
{
    //If a write operation is already in progress, do nothing
    if(impl_->write_buffer==0)
    {
        lock_guard<mutex> l(impl_->write_queue_mutex);
        impl_->writer_buffer_size=impl_->write_queue.size();
        impl_->write_buffer.reset(new char[impl_->write_queue.size()]);
        copy(impl_->write_queue.begin(),impl_->write_queue.end(),
                impl_->write_buffer.get());
        impl_->write_queue.clear();
        async_write(impl_->port,asio::buffer(impl_->write_buffer.get(),
                impl_->writer_buffer_size),
                boost::bind(&async_serial::write_end, this, asio::placeholders::error));
    }
}

void async_serial::write_end(const boost::system::error_code& error)
{
    if(!error)
    {
        lock_guard<mutex> l(impl_->write_queue_mutex);
        if(impl_->write_queue.empty())
        {
            impl_->write_buffer.reset();
            impl_->writer_buffer_size=0;
            
            return;
        }
        impl_->writer_buffer_size=impl_->write_queue.size();
        impl_->write_buffer.reset(new char[impl_->write_queue.size()]);
        copy(impl_->write_queue.begin(),impl_->write_queue.end(),
                impl_->write_buffer.get());
        impl_->write_queue.clear();
        async_write(impl_->port,asio::buffer(impl_->write_buffer.get(),
                impl_->writer_buffer_size),
                boost::bind(&async_serial::write_end, this, asio::placeholders::error));
    } else {
        set_error_status(true);
        do_close();
    }
}

void async_serial::do_close()
{
    boost::system::error_code ec;
    impl_->port.cancel(ec);
    if(ec) set_error_status(true);
    impl_->port.close(ec);
    if(ec) set_error_status(true);
}

void async_serial::set_error_status(bool e)
{
    lock_guard<mutex> l(impl_->error_mutex);
    impl_->error=e;
}

void async_serial::set_read_callback(const std::function<void (const char*, size_t)>& callback)
{
    impl_->callback=callback;
}

void async_serial::clear_read_callback()
{
    std::function<void (const char*, size_t)> empty;
    impl_->callback.swap(empty);
}

#else //__APPLE__

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

class async_serial_impl: private boost::noncopyable
{
public:
    async_serial_impl(): bg_thread(), open(false), error(false) {}

    boost::thread bg_thread; ///< Thread that runs read operations
    bool open; ///< True if port open
    bool error; ///< Error flag
    mutable boost::mutex error_mutex; ///< Mutex for access to error

    int fd; ///< File descriptor for serial port
    
    char read_buffer[async_serial::read_buffer_size]; ///< data being read

    /// Read complete callback
    std::function<void (const char*, size_t)> callback;
};

async_serial::async_serial(): impl_(new async_serial_impl)
{

}

async_serial::async_serial(const std::string& devname, unsigned int baud_rate,
        asio::serial_port_base::parity opt_parity,
        asio::serial_port_base::character_size opt_csize,
        asio::serial_port_base::flow_control opt_flow,
        asio::serial_port_base::stop_bits opt_stop)
        : impl_(new async_serial_impl)
{
    open(devname,baud_rate,opt_parity,opt_csize,opt_flow,opt_stop);
}

void async_serial::open(const std::string& devname, unsigned int baud_rate,
        asio::serial_port_base::parity opt_parity,
        asio::serial_port_base::character_size opt_csize,
        asio::serial_port_base::flow_control opt_flow,
        asio::serial_port_base::stop_bits opt_stop)
{
    if(is_open()) close();

    set_error_status(true);//If an exception is thrown, error remains true
    
    struct termios new_attributes;
    speed_t speed;
    int status;
    
    // Open port
    impl_->fd=::open(devname.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (impl_->fd<0) throw(boost::system::system_error(
            boost::system::error_code(),"Failed to open port"));
    
    // Set Port parameters.
    status=tcgetattr(impl_->fd,&new_attributes);
    if(status<0  || !isatty(impl_->fd))
    {
        ::close(impl_->fd);
        throw(boost::system::system_error(
                    boost::system::error_code(),"Device is not a tty"));
    }
    new_attributes.c_iflag = IGNBRK;
    new_attributes.c_oflag = 0;
    new_attributes.c_lflag = 0;
    new_attributes.c_cflag = (CS8 | CREAD | CLOCAL);//8 data bit,Enable receiver,Ignore modem
    /* In non canonical mode (Ctrl-C and other disabled, no echo,...) VMIN and VTIME work this way:
    if the function read() has'nt read at least VMIN chars it waits until has read at least VMIN
    chars (even if VTIME timeout expires); once it has read at least vmin chars, if subsequent
    chars do not arrive before VTIME expires, it returns error; if a char arrives, it resets the
    timeout, so the internal timer will again start from zero (for the nex char,if any)*/
    new_attributes.c_cc[VMIN]=1;// Minimum number of characters to read before returning error
    new_attributes.c_cc[VTIME]=1;// Set timeouts in tenths of second

    // Set baud rate
    switch(baud_rate)
    {
        case 50:speed= B50; break;
        case 75:speed= B75; break;
        case 110:speed= B110; break;
        case 134:speed= B134; break;
        case 150:speed= B150; break;
        case 200:speed= B200; break;
        case 300:speed= B300; break;
        case 600:speed= B600; break;
        case 1200:speed= B1200; break;
        case 1800:speed= B1800; break;
        case 2400:speed= B2400; break;
        case 4800:speed= B4800; break;
        case 9600:speed= B9600; break;
        case 19200:speed= B19200; break;
        case 38400:speed= B38400; break;
        case 57600:speed= B57600; break;
        case 115200:speed= B115200; break;
        case 230400:speed= B230400; break;
        default:
        {
            ::close(impl_->fd);
            throw(boost::system::system_error(
                        boost::system::error_code(),"Unsupported baud rate"));
        }
    }

    cfsetospeed(&new_attributes,speed);
    cfsetispeed(&new_attributes,speed);

    //Make changes effective
    status=tcsetattr(impl_->fd, TCSANOW, &new_attributes);
    if(status<0)
    {
        ::close(impl_->fd);
        throw(boost::system::system_error(
                    boost::system::error_code(),"Can't set port attributes"));
    }

    //These 3 lines clear the O_NONBLOCK flag
    status=fcntl(impl_->fd, F_GETFL, 0);
    if(status!=-1) fcntl(impl_->fd, F_SETFL, status & ~O_NONBLOCK);

    set_error_status(false);//If we get here, no error
    impl_->open=true; //Port is now open

    thread t(bind(&async_serial::do_read, this));
    impl_->bg_thread.swap(t);
}

bool async_serial::is_open() const
{
    return impl_->open;
}

bool async_serial::error_status() const
{
    lock_guard<mutex> l(impl_->error_mutex);
    return impl_->error;
}

void async_serial::close()
{
    if(!is_open()) return;

    impl_->open=false;

    ::close(impl_->fd); //The thread waiting on I/O should return

    impl_->bg_thread.join();
    if(error_status())
    {
        throw(boost::system::system_error(boost::system::error_code(),
                "Error while closing the device"));
    }
}

void async_serial::write(const char *data, size_t size)
{
    if(::write(impl_->fd,data,size)!=size) set_error_status(true);
}

void async_serial::write(const std::vector<char>& data)
{
    if(::write(impl_->fd,&data[0],data.size())!=data.size())
        set_error_status(true);
}

void async_serial::write(const std::string& s)
{
    if(::write(impl_->fd,&s[0],s.size())!=s.size()) set_error_status(true);
}

async_serial::~async_serial()
{
    if(is_open())
    {
        try {
            close();
        } catch(...)
        {
            //Don't throw from a destructor
        }
    }
}

void async_serial::do_read()
{
    //Read loop in spawned thread
    for(;;)
    {
        int received=::read(impl_->fd,impl_->read_buffer,read_buffer_size);
        if(received<0)
        {
            if(is_open()==false) return; //Thread interrupted because port closed
            else {
                set_error_status(true);
                continue;
            }
        }
        if(impl_->callback) impl_->callback(impl_->read_buffer, received);
    }
}

void async_serial::read_end(const boost::system::error_code& error,
        size_t bytes_transferred)
{
    //Not used
}

void async_serial::do_write()
{
    //Not used
}

void async_serial::write_end(const boost::system::error_code& error)
{
    //Not used
}

void async_serial::do_close()
{
    //Not used
}

void async_serial::set_error_status(bool e)
{
    lock_guard<mutex> l(impl_->error_mutex);
    impl_->error=e;
}

void async_serial::set_read_callback(const std::function<void (const char*, size_t)>& callback)
{
    impl_->callback=callback;
}

void async_serial::clear_read_callback()
{
    std::function<void (const char*, size_t)> empty;
    impl_->callback.swap(empty);
}

#endif //__APPLE__

//
//Class callback_async_serial
//

callback_async_serial::callback_async_serial(): async_serial()
{

}

callback_async_serial::callback_async_serial(const std::string& devname,
        unsigned int baud_rate,
        asio::serial_port_base::parity opt_parity,
        asio::serial_port_base::character_size opt_csize,
        asio::serial_port_base::flow_control opt_flow,
        asio::serial_port_base::stop_bits opt_stop)
        :async_serial(devname,baud_rate,opt_parity,opt_csize,opt_flow,opt_stop)
{

}

void callback_async_serial::set_callback(const std::function<void (const char*, size_t)>& callback)
{
    set_read_callback(callback);
}

void callback_async_serial::clear_callback()
{
    clear_read_callback();
}

callback_async_serial::~callback_async_serial()
{
    clear_read_callback();
}
