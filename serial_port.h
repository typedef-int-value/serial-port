/*
 * File:   serial_port.h
 * Author: Terraneo Federico
 * Distributed under the Boost Software License, Version 1.0.
 *
 * Created on September 12, 2009, 3:47 PM
 * Changed by Seongbeom Kang from Jan 16, 2020
 */

#ifndef __SERIAL_PORT_H_
#define __SERIAL_PORT_H_

#include <boost/asio.hpp>
#include <boost/utility.hpp>
#include <stdexcept>

namespace serial {
/**
 * Thrown if timeout occurs
 */
class timeout_exception : public std::runtime_error {
 public:
  timeout_exception(const std::string& arg) : runtime_error(arg) {}
};

/**
 * Serial port class, with timeout on read operations.
 */
class serial_port : private boost::noncopyable {
 public:
  serial_port();

  /**
   * Opens a serial device. By default timeout is disabled.
   * \param devname serial device name, example "/dev/ttyS0" or "COM1"
   * \param baud_rate serial baud rate
   * \param opt_parity serial parity, default none
   * \param opt_csize serial character size, default 8bit
   * \param opt_flow serial flow control, default none
   * \param opt_stop serial stop bits, default 1
   * \throws boost::system::system_error if cannot open the
   * serial device
   */
  serial_port(const std::string& devname, unsigned int baud_rate,
              boost::asio::serial_port_base::parity opt_parity =
                  boost::asio::serial_port_base::parity(
                      boost::asio::serial_port_base::parity::none),
              boost::asio::serial_port_base::character_size opt_csize =
                  boost::asio::serial_port_base::character_size(8),
              boost::asio::serial_port_base::flow_control opt_flow =
                  boost::asio::serial_port_base::flow_control(
                      boost::asio::serial_port_base::flow_control::none),
              boost::asio::serial_port_base::stop_bits opt_stop =
                  boost::asio::serial_port_base::stop_bits(
                      boost::asio::serial_port_base::stop_bits::one));

  /**
   * Opens a serial device.
   * \param devname serial device name, example "/dev/ttyS0" or "COM1"
   * \param baud_rate serial baud rate
   * \param opt_parity serial parity, default none
   * \param opt_csize serial character size, default 8bit
   * \param opt_flow serial flow control, default none
   * \param opt_stop serial stop bits, default 1
   * \throws boost::system::system_error if cannot open the
   * serial device
   */
  void open(const std::string& devname, unsigned int baud_rate,
            boost::asio::serial_port_base::parity opt_parity =
                boost::asio::serial_port_base::parity(
                    boost::asio::serial_port_base::parity::none),
            boost::asio::serial_port_base::character_size opt_csize =
                boost::asio::serial_port_base::character_size(8),
            boost::asio::serial_port_base::flow_control opt_flow =
                boost::asio::serial_port_base::flow_control(
                    boost::asio::serial_port_base::flow_control::none),
            boost::asio::serial_port_base::stop_bits opt_stop =
                boost::asio::serial_port_base::stop_bits(
                    boost::asio::serial_port_base::stop_bits::one));

  /**
   * \return true if serial device is open
   */
  bool is_open() const;

  /**
   * Close the serial device
   * \throws boost::system::system_error if any error
   */
  void close();

  /**
   * Set the timeout on read/write operations.
   * To disable the timeout, call setTimeout(boost::posix_time::seconds(0));
   */
  void set_timeout(const boost::posix_time::time_duration& t);

  /**
   * Write data
   * \param data array of char to be sent through the serial device
   * \throws boost::system::system_error if any error
   */
  void write(const char* data, size_t size);
  void write(const std::vector<unsigned char>& data);
  void write(const std::vector<char>& data);
  void write(const std::string& s);

  /**
   * Read some data, blocking
   * \param data array of char to be read through the serial device
   * \param size array size
   * \return numbr of character actually read 0<=return<=size
   * \throws boost::system::system_error if any error
   * \throws timeout_exception in case of timeout
   */
  void read(char* data, size_t size);

  /* read until timeout, blocking */
  std::vector<char> read_until_timeout(size_t max_capacity = 65535);

  void set_baudrate(int bps);

  ~serial_port();

 private:
  /**
   * Parameters of performReadSetup.
   * Just wrapper class, no encapsulation provided
   */
  class read_setup {
   public:
    read_setup() : data_(0), size_(0) {}

    read_setup(char* data, size_t size) : data_(data), size_(size) {}

    // Using default copy constructor, operator=
    char* data_;
    size_t size_;
  };

  /**
   * Callack called either when the read timeout is expired or canceled.
   * If called because timeout expired, sets result to resultTimeoutExpired
   */
  void timeout_expired(const boost::system::error_code& error);

  /**
   * Callback called either if a read complete or read error occurs
   * If called because of read complete, sets result to resultSuccess
   * If called because read error, sets result to resultError
   */
  void read_complete(const boost::system::error_code& error,
                     const size_t byte_transferred);

  /**
   * Possible outcome of a read. Set by callbacks, read from main code
   */
  enum class read_result { in_progress, success, error, timeout };

  boost::asio::io_service io_;                ///< Io service object
  boost::asio::serial_port port_;             ///< Serial port object
  boost::asio::deadline_timer timer_;         ///< Timer for timeout
  boost::posix_time::time_duration timeout_;  ///< Read/write timeout
  enum read_result read_result_;      ///< Used by read with timeout
  read_setup read_setup_;             ///< Global because used in the OSX fix
};
}  // namespace serial
#endif
